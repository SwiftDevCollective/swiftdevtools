# Swift Dev Tools

Gathering information about, and supporting projects for,
tools for software development in the Swift programming language,
and the Apple platforms in general.

See
[the project wiki](https://gitlab.com/SwiftDevCollective/swiftdevtools/-/wikis/home)
for ongoing updates.

## Authors and acknowledgment

- [Grant Neufeld](https://gitlab.com/grantneufeld) ([Linktree](https://linktr.ee/grantzero))

## License

Copyright ©2024 Grant Neufeld, et. al.

The documentation of this project is licensed under the terms of the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license.
[CC BY-NC-SA 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1)

This license requires that reusers give credit to the creator.
It allows reusers to distribute, remix, adapt,
and build upon the material in any medium or format,
for noncommercial purposes only.
If others modify or adapt the material,
they must license the modified material under identical terms.
